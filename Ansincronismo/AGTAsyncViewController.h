//
//  AGTAsyncViewController.h
//  Ansincronismo
//
//  Created by Fernando Rodríguez Romero on 21/01/15.
//  Copyright (c) 2015 Agbo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AGTAsyncViewController : UIViewController

@property (nonatomic, weak) IBOutlet UIImageView *photoView;

-(IBAction)syncDownload:(id)sender;
-(IBAction)asyncDownload:(id)sender;
-(IBAction)asyncPlusDownload:(id)sender;

@end

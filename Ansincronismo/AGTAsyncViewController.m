//
//  AGTAsyncViewController.m
//  Ansincronismo
//
//  Created by Fernando Rodríguez Romero on 21/01/15.
//  Copyright (c) 2015 Agbo. All rights reserved.
//

#import "AGTAsyncViewController.h"

@interface AGTAsyncViewController ()

@end

@implementation AGTAsyncViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)syncDownload:(id)sender{
    
    self.photoView.image = nil;
    
    NSURL *url = [NSURL URLWithString:@"http://elhorizonte.mx/foto/470000/475983_9.jpg"];
    
    NSData *data = [NSData dataWithContentsOfURL:url];
    
    UIImage *img = [UIImage imageWithData:data];
    
    self.photoView.image = img;
    
    
}
-(IBAction)asyncDownload:(id)sender{
    
    dispatch_queue_t gemelas = dispatch_queue_create("davalos", 0);
    
    __block UIImage *img = nil;
    
    dispatch_async(gemelas, ^{
        // Nos vamos a segundo plano
        NSURL *url = [NSURL URLWithString:@"http://elhorizonte.mx/foto/470000/475983_9.jpg"];
        NSData *data = [NSData dataWithContentsOfURL:url];
        img = [UIImage imageWithData:data];
        
        // Nos vamos a primer plano
        dispatch_async(dispatch_get_main_queue(), ^{
           self.photoView.image = img;
        });
        
    });
    

    
}
-(IBAction)asyncPlusDownload:(id)sender{
    
    [self withGemelas:^(UIImage *gemelas) {
        self.photoView.image = gemelas;
    }];
    
}


-(void)withGemelas:(void (^)(UIImage* gemelas))completionBlock{
    
    // Nos vamos a segundo plano
    
    dispatch_async(dispatch_get_global_queue(QOS_CLASS_USER_INTERACTIVE, 0), ^{
        
        NSURL *url = [NSURL URLWithString:@"http://elhorizonte.mx/foto/470000/475983_9.jpg"];
        
        NSData *data = [NSData dataWithContentsOfURL:url];
        
        UIImage *img = [UIImage imageWithData:data];
        
        // Ya tengo a las gemelas
        // las paso al bloque de finalización
        // SIEMPRE ejecutaremos los bloque de finalización
        // en la cola proncipal.
        dispatch_async(dispatch_get_main_queue(), ^{
            completionBlock(img);
        });
    });
 
}











@end
